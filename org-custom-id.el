;;; org-custom-id.el --- CUSTOM_ID helpers for Org mode

;; org-custom-id.el Copyright (C) 2018 by Marko Crnić

;; Author: Marko Crnić
;; Keywords: org, link
;; Homepage: https://gitlab.com/weshmashian/org-custom-id
;; Version: 0.1.5

;; This file is not part of GNU Emacs.

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Fast and easy handling of `CUSTOM_ID' properties.

;; Usage:

;; - `org-custom-id-get-create' creates a new `CUSTOM_ID' for heading at point
;; - `org-custom-id-insert-link' uses read-completion to insert link pointing to a specific `CUSTOM_ID'
;; - `org-custom-id-update-buffer' creates missing `CUSTOM_ID' properties

;; See docstring for above functions for more details.

;;; Code:
(require 'org)
(require 'org-id)
(require 'org-element)

;; Constructing portable heading IDs
;; One way to solve this is by using `CUSTOM_ID' property for each heading. Doing this manually is
;; tedious and prone to errors, and since headings can change names, we need to be able to update any
;; existing IDs to reflect this in order not to break links.

;; This is heavily based on `org-id'. Logic is a bit simpler since we do not need to keep track of
;; custom IDs over several files.

(defgroup org-custom-id nil
  "Options for creating CUSTOM_ID properties."
  :tag "Org Custom ID"
  :group 'org)

(defcustom org-custom-id-new-fname 'org-custom-id-new
  "Function used to construct value of CUSTOM_ID.

Should accept one optional parameter, PREFIX."
  :group 'org-custom-id
  :type 'symbol)

(defun org-custom-id-new (&optional prefix)
  "Create a new CUSTOM_ID.

CUSTOM_ID consists of optional PREFIX, and ITEM property stripped
of any whitespace."
   (replace-regexp-in-string
    "\s" ""
    (concat
     (when prefix prefix)
     (org-entry-get nil "ITEM"))))

(defun org-custom-id-get (&optional pom create prefix)
  "Get the CUSTOM_ID property of the entry at point-or-marker POM.

If POM is nil, refer to the entry at point.

If the entry does not have a CUSTOM_ID, the function returns nil.

However, when CREATE is non nil, create a CUSTOM_ID if none is
present already. PREFIX will be passed through to function set in
`org-custom-id-new-fname'.

If REPLACE is non-nil find and replace all references to existing
CUSTOM_ID with the new one.

In any case, the CUSTOM_ID of the entry is returned."
  (org-with-point-at pom
    (let ((cid (org-entry-get nil "CUSTOM_ID")))
      (cond
       ((and cid (string-match "\\S-" cid)) cid)
       (create
        (setq cid (funcall org-custom-id-new-fname prefix))
        (org-entry-put pom "CUSTOM_ID" cid)
        cid)))))

;;;###autoload
(defun org-custom-id-get-create (&optional force)
  "Create a CUSTOM_ID for the current entry and return it.

If the entry already has a CUSTOM_ID, just return it.

If FORCE is `\\[universal-argument]' or non-nil, force the creation of a new CUSTOM_ID.

If FORCE is `\\[universal-argument] \\[universal-argument]', force the creation of a new CUSTOM_ID,
and update existing links found in buffer."
  (interactive "P")
  (let (current old)
    (setq old (org-custom-id-get (point)))
    (when force
      (org-entry-put (point) "CUSTOM_ID" nil))
    (org-custom-id-get (point) 'create)
    (when (equal force '(16))
      (save-excursion
        (setq current (org-custom-id-get (point)))
        (org-custom-id-update old current)))))

;;;###autoload
(defun org-custom-id-update-buffer (&optional force)
  "Create CUSTOM_ID property for headings in buffer that do not already have it.

If FORCE is `\\[universal-argument]' or non-nil, force the creation of a new CUSTOM_ID.

If FORCE is `\\[universal-argument] \\[universal-argument]', force the creation of a new CUSTOM_ID,
and update existing links found in buffer."
  (interactive "P")
  (org-map-entries (lambda () (org-custom-id-get-create force)) t nil))

;; Updating existing links
;; Calling `org-custom-id-get-create' with double universal-argument (`C-u C-u') will also call the
;; following function.

(defun org-custom-id-update (old new)
  "Search for all custom-id links to OLD and replace with NEW."
  (let (elt pos (case-fold-search t) (count 0))
    (save-excursion
      (goto-char (point-min))
      (while (search-forward-regexp org-link-bracket-re nil 'noerror)
        (setq elt (org-element-context))
        (when (and (string= (org-element-property :type elt) "custom-id")
                   (string= (org-element-property :path elt) old))
          (save-match-data
            (setq pos (org-in-regexp org-link-bracket-re))
            (incf count)
            (search-forward old (cdr pos) 'noerror)
            (replace-match (concat "#" new) 'fixedcase 'literal nil 1)))))
    (message "%s link(s) updated." count)))

;; Custom ID auto-completion
;; We need to generate a list containing values of all present `CUSTOM_ID' properties so we can use it
;; with completing read and create a valid Org link.

;; Org-id already has a function that handles outline completion nicely. We can reuse that, but we will
;; need to "redirect" `org-id-get' calls to our `org-custom-id-get' function.

(defun org-custom-id-get-with-outline-path-completion (&optional targets)
  "Use `outline-path-completion' to retrieve CUSTOM_ID of an entry.

Function uses `org-id-get-with-outline-path-completion' with
calls to `org-id-get' replaced with calls to
`org-custom-id-get'.

TARGETS may be a setting for `org-refile-targets' to define
eligible headlines."
  (cl-letf (((symbol-function 'org-id-get) #'org-custom-id-get))
    (funcall 'org-id-get-with-outline-path-completion targets)))

;; Now we can collect all the properties and use them in an interactive function.

(defun org-custom-ids ()
  "Return a list of CUSTOM_ID property values found in file."
  (delq nil (org-map-entries (lambda () (org-entry-get (point) "CUSTOM_ID")) t nil)))

(defun org-custom-id--get-link-data ()
  "Return a list containing link target, description and cons
cell with start end end positions."
  (let ((pos (org-in-regexp org-link-bracket-re))
        link description)
    (when pos
      (setq link (buffer-substring-no-properties (match-beginning 1) (match-end 1))
            description (or (ignore-errors (buffer-substring-no-properties (match-beginning 3) (match-end 3))) link))
      (list link description pos))))

;;;###autoload
(defun org-custom-id-insert-link (&optional target description)
  "Make a bracketed link to TARGET using DESCRIPTION.

If necessary, CUSTOM_ID property will be created on target."
  (interactive)
  (let* ((link (if target target (concat "#" (org-custom-id-get-with-outline-path-completion))))
         (linkdata (org-custom-id--get-link-data))
         (orgregion (org-region-active-p))
         (linkbeg (cond (linkdata (caar (last linkdata)))
                        (orgregion (region-beginning))))
         (linkend (cond (linkdata (cdar (last linkdata)))
                        (orgregion (region-end))))
         (_description (cond
                        (linkdata (or (nth 1 linkdata) link))
                        (orgregion (buffer-substring-no-properties (region-beginning) (region-end)))))
         (_default (or description (read-string
                                    (format "Description%s"
                                            (if _description (format " (default %s): " _description) ": "))
                                    nil nil _description)))
         (_link (org-link-make-string link _default)))
    (when linkend (delete-region linkbeg linkend))
    (insert _link)))

;;;###autoload
(defun org-custom-id-lint-interactive ()
  "Check if all custom-id links in buffer point to valid target and ask for change."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (let (link linkdata)
      (while (search-forward-regexp org-link-bracket-re nil 'noerror)
        (setq link (org-element-context))
        (when (and
               (string= "custom-id" (org-element-property :type link))
               (not (org-find-property "CUSTOM_ID" (org-element-property :path link))))
          (setq linkdata (org-custom-id--get-link-data))
          (when (y-or-n-p
                 (format "\"%s\" points to unknown CUSTOM_ID: %s. Edit? " (nth 1 linkdata) (nth 0 linkdata)))
            (org-custom-id-insert-link)))))))

(provide 'org-custom-id)
;;; org-custom-id.el ends here
